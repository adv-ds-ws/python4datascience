parse_income = """
religion_stack["income_split"] = religion_stack["income"].apply(lambda x: re.findall("(\$[\d,]+)?-?[\w\s]*(\$[\d,]+)?[\w\s]*", x)[0])
religion_stack["income_lower"] = religion_stack["income_split"].apply(lambda x: x[0])
religion_stack["income_upper"] = religion_stack["income_split"].apply(lambda x: x[1])
"""