random_sample = """mtcars.sample(n=7, random_state=3)""" 

memory_optimization = """import copy 
mtcars_red = copy.deepcopy(mtcars)
mtcars_red["cyl"] = mtcars_red["cyl"].astype(np.uint8)
mtcars_red["gear"] = mtcars_red["gear"].astype("uint8")
mtcars_red["hp"] = mtcars_red["hp"].astype("int16")
mtcars_red["am"] = mtcars_red["am"].astype("category")
mtcars_red.info()

"""
select_float64 = """mtcars.loc[:,mtcars.dtypes == 'float64']"""

two_boxplots = """mtcars[["cyl","drat","wt", "qsec", "vs", "am", "gear", "carb"]].boxplot()"""

smaller_scatter_matrix = """scatter_matrix(mtcars[["cyl", "hp", "disp"]], alpha=0.9,figsize=(15, 15), diagonal='kde', s = 300)"""

smart_scatterplot = """ax = mtcars.plot.scatter(x='hp', y='mpg',s=mtcars['cyl']*10, c="wt", label='4',colormap="CMRmap", figsize=(12, 6));"""


beautiful_plot = """from pandas.plotting import andrews_curves
plt.figure()
andrews_curves(mtcars, 'cyl')"""

# Linear regression
linear_regression_grown = """X_train, X_test, y_train, y_test = train_test_split(mtcars[["qsec","wt"]], 
                                                           mtcars["hp"], test_size=0.33, random_state=42)


reg = linear_model.LinearRegression(fit_intercept=True, n_jobs=1, normalize=False)
reg.fit (X_train, y_train)

pred = reg.predict(X_test)
def mean_absolute_percentage_error(y_test, pred):
    return np.mean(np.abs((y_test - pred) / y_test))*100




#print(f'Intercept: {reg.intercept_:.4f}')
print('Coefficients: \n', reg.coef_)
print('Intersect: \n', reg.intercept_)
print(f"Mean squared error: {mean_squared_error(y_test, pred):.2f}")
print(f"Mean absolute error: {mean_absolute_error(y_test, pred):.2f}")
print(f"Mean absolute percentage error: {mean_absolute_percentage_error(y_test, pred):.2f} ")
print(f'R^2: {r2_score(y_test, pred):.2f}') 
"""

# bad data

unknown_data = """nb = 6
n = nb * nb

ig1, axs1 = plt.subplots(nrows=nb,ncols=nb,figsize=(16,16))
for i in range(0,n):
    
    cx = i%nb
    cy = int(i/nb)
    
    ax = axs1[cx,cy]
    np.random.seed(i)
    sns.regplot(np.arange(10),np.random.rand(10), ax = ax).set_title(f'{i}')"""

#####
logistic_regression_grown = """#The solution is better but not perfect! Convergence takes much longer now.
diam_dummies = pd.get_dummies(diamonds[["color", "clarity"]], columns =["color", "clarity"], drop_first=True)
#diamonds_merged = pd.merge(diamonds, diam_dummies,left_index=True, right_index=True) We don't really need those
diamonds_merged = diamonds
reg = linear_model.LogisticRegression(solver='lbfgs', multi_class='auto',max_iter=3000)

X_train, X_test, y_train, y_test = train_test_split(diamonds_merged[["x","y","z","depth","table"]], 
                                                    diamonds_merged["cut_num"].astype(int), 
                                                    test_size=0.20, 
                                                    random_state=42)
reg.fit(X_train, y_train) 
pred = reg.predict(X_test)
report = classification_report(y_test, pred, target_names=["Fair", "Good", "Very Good", "Premium", "Ideal"])
print(report)
comparison_logreg_grown = pd.DataFrame({"real":y_test.values, 
                           "pred":np.round(pred, 2), 
                           "match":y_test.values==pred}, 
                          index=y_test.index)[["real", "pred", "match"]]

report_overview["logreg_grown"] = report"""

tree_grown = """diam_dummies = pd.get_dummies(diamonds[["color", "clarity"]], columns =["color", "clarity"], drop_first=True)
diamonds_merged = pd.merge(diamonds, diam_dummies,left_index=True, right_index=True)

var_names = ["price","carat","x","y","z","depth", "table"]+list(diam_dummies.columns)

diamonds_data = diamonds_merged[var_names]
diamonds_target = diamonds["cut_num"].astype(int)

#diamonds_data = diamonds.loc[:, diamonds.columns != "cut_num"]


X_train, X_test, y_train, y_test = train_test_split(diamonds_data, 
                                                    diamonds_target, 
                                                    test_size=0.20, 
                                                    random_state=42)

decision_tree_classifier = tree.DecisionTreeClassifier()
decision_tree_classifier.fit(X_train, y_train)

pred = decision_tree_classifier.predict(X_test)
report = classification_report(y_test, pred, target_names=["Fair", "Good", "Very Good", "Premium", "Ideal"])
print(report)
comparison_tree_grown = pd.DataFrame({"real":y_test.values, 
                           "pred":np.round(pred, 2), 
                           "match":y_test.values==pred}, 
                          index=y_test.index)[["real", "pred", "match"]]

report_overview["tree_grown"] = report"""

random_forest = """from sklearn import ensemble
diam_dummies = pd.get_dummies(diamonds[["color", "clarity"]], columns =["color", "clarity"], drop_first=True)
diamonds_merged = pd.merge(diamonds, diam_dummies,left_index=True, right_index=True)

var_names = ["price","carat","x","y","z","depth", "table"]+list(diam_dummies.columns)

diamonds_data = diamonds_merged[var_names]
diamonds_target = diamonds["cut_num"].astype(int)

#diamonds_data = diamonds.loc[:, diamonds.columns != "cut_num"]


X_train, X_test, y_train, y_test = train_test_split(diamonds_data, 
                                                    diamonds_target, 
                                                    test_size=0.20, 
                                                    random_state=42)

random_forest_classifier = ensemble.RandomForestClassifier(n_estimators = 100)
random_forest_classifier.fit(X_train, y_train)

pred = random_forest_classifier.predict(X_test)
report = classification_report(y_test, pred, target_names=["Fair", "Good", "Very Good", "Premium", "Ideal"])
print(report)
report_overview["random_forest"] = report

comparison_rf = pd.DataFrame({"real":y_test.values, 
                           "pred":np.round(pred, 2), 
                           "match":y_test.values==pred}, 
                          index=y_test.index)[["real", "pred", "match"]]"""

logreg_pca = """from sklearn.decomposition import PCA

diam_dummies = pd.get_dummies(diamonds[["color", "clarity"]], columns =["color", "clarity"], drop_first=True)
diamonds_merged = pd.merge(diamonds, diam_dummies,left_index=True, right_index=True)# We don't really need those
#diamonds_merged = diamonds
reg = linear_model.LogisticRegression(solver='lbfgs', multi_class='auto',max_iter=3000)


var_names = ["price","carat","x","y","z","table","depth"]+list(diam_dummies.columns)

diamonds_data = diamonds_merged[var_names]
diamonds_target = diamonds["cut_num"].astype(int)

#diamonds_data = diamonds.loc[:, diamonds.columns != "cut_num"]


X_train, X_test, y_train, y_test = train_test_split(diamonds_data, 
                                                    diamonds_target, 
                                                    test_size=0.20, 
                                                    random_state=42)

#run the pca on the components
pca = PCA(n_components=8) # change this number for better cat picture!
pca.fit(X_train)
X_test_pca = pca.transform(X_test)
X_train_pca = pca.transform(X_train)


#X_test.shape

#With PCA
print("With PCA")
reg = linear_model.LogisticRegression(solver='lbfgs', multi_class='auto',max_iter=3000)
reg.fit(X_train_pca, y_train) 
pred = reg.predict(X_test_pca)
report= classification_report(y_test, pred, target_names=["Fair", "Good", "Very Good", "Premium", "Ideal"])
print(report)
report_overview["logreg_pca"] = report """


#####
min_by_year_a = """storm_counts_by_year_and_state = storms.groupby(["YEAR", "STATE"]).size().reset_index(name='counts')
storm_counts_by_year_and_state.head(3)"""
min_by_year_b  = """ids = storm_counts_by_year_and_state.groupby("YEAR").idxmin()
storm_counts_by_year_and_state.iloc[ids["counts"], :]"""

#####
students_a = """students.loc[students["accepted"]=="YES", "accepted_num"] = 1
students.loc[students["accepted"]=="NO", "accepted_num"] = 0
students.head(5)"""
students_b = """students.groupby("gender")["accepted_num"].mean()"""
students_c = """students_by_dep = students.groupby(["gender", "department"])["accepted_num"].mean()
students_by_dep"""
students_d = """students_by_dep.index"""
students_e = """students_by_dep_m_f = pd.DataFrame({"female":students_by_dep["FEMALE"].values, 
                                    "male":students_by_dep["MALE"].values}, 
                                   index = students_by_dep["MALE"].index)
students_by_dep_m_f["female_bias"] = (students_by_dep_m_f["female"]-students_by_dep_m_f["male"])/students_by_dep_m_f["female"]
students_by_dep_m_f"""

#####
prices_a = """prices.head(3)"""
prices_b = """prices.plot(figsize=(14,6))"""
prices_c = """pd.DataFrame(np.corrcoef(prices.iloc[:, 1:].values.T))"""
prices_d = """pd.DataFrame(np.corrcoef(prices.iloc[:, 1:].diff().iloc[1:, :].values.T))"""
prices_e = """prices.iloc[:, 1:].diff().plot(alpha=.5, figsize=(14,6))"""

#####
hawaii_a = """states = storms["STATE"].unique()
states.sort()
print(states)
print(len(states))"""
hawaii_b = """storm_counts_by_year_and_state[["YEAR", "STATE"]].groupby("YEAR").size().reset_index(name='counts')"""
hawaii_c = """states_w_storms_2017 = storm_counts_by_year_and_state[storm_counts_by_year_and_state["YEAR"] == 2017]["STATE"].unique()
set(states).difference(states_w_storms_2017)"""


